# Работа в bitrix

## Ошибки

Ошибка при обращении к БД содержит
```
which is not in SELECT list; this is incompatible with DISTINCT]
```
Решение
```sql
SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
SELECT @@sql_mode;
```
